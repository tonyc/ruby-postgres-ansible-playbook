# ruby-postgres-ansible-boilerplate

Basic modular ansible playbook for setting up ubuntu with ansible for ruby and postgres.

There's a bit of work left to do (proper application server setup, unicorn,etc), but the basic structure is here for a useful development environment in vagrant.

This repo is designed to be dropped into an existing Rails application. There's also a sample Vagrantfile included for reference.

## Setup
* Set `app_name` in `ansible/web.yml`. This is used for where nginx will be pointed at, eventually.

* Edit `ansible/roles/appuser/tasks/main.yml` And add your username and a public key per the comments. Your public key should go into `ansible/public_keys/username`

* `vagrant up`

